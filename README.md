# idcf_webclient_go

## Description
IDCF Cloudへログインして障害情報を取得する

## Usage

* create auth config file

  $ vi ~/.config/idcf.yml
  user: USER_NAME
  pass: PASSWORD

* request
 
  $ idcf_webclient_go




sync.WaitGroupは複数のgoroutineの完了を待つための値

  1. WaitGroupの値に対してメソッドWaitを呼ぶと、WaitGroupが0になるまでWaitはブロックされる（待たされる）。
     処理の数だけWaitGroupの値をインクリメントしておいて、処理完了時にデクリメントすれば、Waitを呼んで処理完了を待っているメインのgoroutineは、すべての処理が完了する


