package main

import (
	"fmt"
	"webclient_go/idcf"
)

func main() {
	err := idcf.MaintenanceCheck()
	if err != nil {
		fmt.Println(err)
	}
}
