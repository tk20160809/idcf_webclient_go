module webclient_go

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
)
