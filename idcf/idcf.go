package idcf

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
	"golang.org/x/sync/errgroup"
	"gopkg.in/yaml.v2"
)

type Client struct {
	//URL        *url.URL
	HTTPClient      *http.Client
	URL             string `yaml:"url"`
	Incident_URL    string `yaml:"incident_url"`
	Maintenance_URL string `yaml:"maintenance_url"`
	Username        string `yaml:"username"`
	Password        string `yaml:"password"`
	Logger          *log.Logger

	// API
	EndPoint  string `yaml:"endpoint"`
	RoomID    string `yaml:"room_id"`
	API_Token string `yaml:"api_token"`
}

var layout = "2006-01-02 15:04:05"
var Config Client

const PollingSec = 3600
const TIMEOUT = 10

// Load ~/.config/idcf.yml
func loadConfig() error {
	home := os.Getenv("HOME")
	configFile := filepath.Join(home, ".config", "idcf.yml")
	buf, err := os.ReadFile(configFile)
	if err != nil {
		return fmt.Errorf("loadConfig() os.ReadFile(): %v\n", err)
	}
	if err := yaml.Unmarshal(buf, &Config); err != nil {
		return fmt.Errorf("loadConfig() yaml.Unmarshal(): %v\n", err)
	}
	return nil
}

// <input type="hidden" name="execution" value= 取得
func parseItem(r io.Reader) (string, error) {
	doc, err := html.Parse(r)
	if err != nil {
		return "", fmt.Errorf("parseItem() html.Parse(): %v\n", err)
	}

	var result string
	var f func(*html.Node)
	f = func(n *html.Node) {
		// n.Typeでノードの型チェック。ElementNodeでHTMLタグのNode。
		// n.Dataでタグチェック
		if n.Type == html.ElementNode && n.Data == "input" {

			// n.Attrで属性を一覧する
			for k, i := range n.Attr {
				if i.Key == "name" && i.Val == "execution" {
					result = n.Attr[k+1].Val
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return result, nil
}

// chatwork 通知
func cwapi(body string) string {
	msg_url := Config.EndPoint + "/rooms/" + Config.RoomID + "/messages"
	v := url.Values{}
	v.Add("body", body)
	req, _ := http.NewRequest("POST", msg_url, strings.NewReader(v.Encode()))
	req.Header.Set("X-ChatWorkToken", Config.API_Token)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	client := http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	api_resp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	return string(api_resp)
}

func idcf_notice(target_url string, logname string) error {

	var jar *cookiejar.Jar
	var err error
	var last_time string

	fmt.Println(target_url, " polling start.")
	for {

		// Cookie Jar
		jar, err = cookiejar.New(nil)
		if err != nil {
			return fmt.Errorf("idcf_notice(): %v\n   ", err)
		}

		// http.Client with Jar   クッキー設定
		c := http.Client{Jar: jar}

		// login ページへアクセスして input hidden name=execution 取得
		execution, err := idcf_login(c)
		if err != nil {
			return fmt.Errorf("idcf_notice() idcf_login(): \n   %v\n", err)
		}

		// login post 処理
		err = idcf_login_post(c, execution)
		if err != nil {
			return fmt.Errorf("idcf_notice(): %v\n   ", err)
		}

		// メンテナンス・障害ページアクセス
		res_body, err := idcf_inquery(c, target_url)
		if err != nil {
			return fmt.Errorf("idcf_notice(): %v\n   ", err)
		}

		// 最新の情報取得
		last_time, err = parseInquery(res_body, last_time, logname)
		if err != nil {
			return err
		}

		// ポーリングチェック
		time.Sleep(time.Second * PollingSec)
	}
}

func idcf_login(c http.Client) (string, error) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*TIMEOUT)
	defer cancel()

	// http.Request with context   login
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, Config.URL, nil)
	if err != nil {
		return "", fmt.Errorf("idcf_login() http.NewRequestWithContext: %v\n", err)
	}

	res, err := c.Do(req)
	if err != nil {
		return "", fmt.Errorf("idcf_login() c.Do: %v\n", err)
	}
	defer res.Body.Close()
	execution, err := parseItem(res.Body)
	if err != nil {
		return "", fmt.Errorf("idcf_login(): %v\n   ", err)
	}

	return execution, nil

}

func idcf_login_post(c http.Client, execution string) error {

	_, err := c.PostForm(Config.URL, url.Values{
		"username":    {Config.Username},
		"password":    {Config.Password},
		"execution":   {execution},
		"_eventId":    {"submit"},
		"geolocation": {""},
	})
	if err != nil {
		return fmt.Errorf("idcf_login_post() c.PostForm(): %v\n", err)
	}

	return nil

}

func idcf_inquery(c http.Client, target_url string) ([]byte, error) {

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*TIMEOUT)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, target_url, nil)
	if err != nil {
		return nil, fmt.Errorf("idcf_inquery() http.NewRequestWithContext(): %v\n", err)
	}
	res, err := c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("idcf_inquery() c.Do(): %v\n", err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("idcf_inquery() res.StatusCode: %v\n", err)
	}

	body, _ := ioutil.ReadAll(res.Body)
	return body, nil
}

func parseInquery(res_body []byte, last_time string, logname string) (string, error) {

	var title, latest_datetime, body string
	f, _ := os.Create(logname)

	res := bytes.NewReader(res_body)
	doc, err := goquery.NewDocumentFromReader(res)
	if err != nil {
		return "", fmt.Errorf("parseInquery() goquery: %v\n", err)
	}

	// <table id="datatables"> .. <tr><td> から取得
	doc.Find("#datatables").Each(func(i int, tr *goquery.Selection) {

		// タイトル
		title = tr.Find("td").Eq(1).Text()

		// 日付
		latest_datetime = tr.Find("td").Eq(2).Text()

		// 本文
		body = tr.Find("td").Eq(3).Text()
	})
	fmt.Println(title)
	fmt.Println(latest_datetime)
	fmt.Println(body)

	// 最新チケットの日付が、前回と変わっていれば通知
	if last_time != "" {
		msg := "Check: " + time.Now().Format(layout) + "\nTitle: " + latest_datetime + " " + strings.TrimLeft(title, "\n") + "\n\n"
		if last_time != latest_datetime {
			msg += body + "\n"
			api_resp := cwapi(msg)
			f.Write([]byte(api_resp + "\n"))
		}
		f.Write([]byte(msg))
	}

	return latest_datetime, nil

}

func MaintenanceCheck() error {

	err := loadConfig()
	if err != nil {
		return fmt.Errorf("MaintenanceCheck()\n   %v\n", err)
	}

	var wg sync.WaitGroup
	eg := new(errgroup.Group)

	wg.Add(1)
	eg.Go(func() error {
		defer wg.Done()
		return idcf_notice(Config.Incident_URL, "incident.log")
	})

	wg.Add(1)
	eg.Go(func() error {
		defer wg.Done()
		return idcf_notice(Config.Maintenance_URL, "maintenance.log")
	})

	wg.Wait()
	if err := eg.Wait(); err != nil {
		return fmt.Errorf("MaintenanceChceck() eg.Wait() %v\n", err)
	}

	return nil
}
